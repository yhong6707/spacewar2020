# SpaceWar2020

## How to install SpaceWar2020
---
- **To compile Project:**    
Install MonoGame 3.7.1 and VS 2019 Community    
MonoGame 3.7.1: https://community.monogame.net/t/monogame-3-7-1/11173    
VS 2019 Community: https://visualstudio.microsoft.com/downloads/    
- **To publish Project:**     
Click Build Menu, and then click "Publish spacewar2020"   
Select publish folder    
Check :From a CD-ROM or DVD-ROM"     
- **To Install Game:**    
Excute setup.exe in publish folder    
Enjoy this game!    


## License
---
A short snippet descibing the licence (**GNU GPLv3**)   
GNU GPLv3 license: https://www.gnu.org/licenses/gpl-3.0.en.html   

GPLv3 (c) Phoenix   

**Note**: I chose **GNU GPLv3** License because I want to share this project with people.    
And, I want to get some enhanced functions made by other developers.    
For these two reasons, I chose the GNU GPLv3 license.   

